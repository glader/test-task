from django.contrib import admin

from storage.models import UserFile


@admin.register(UserFile)
class UserFileAdmin(admin.ModelAdmin):
    list_display = (
        "author",
        "slug",
        "created_at",
    )
    ordering = ("-created_at",)
    search_fields = ["slug"]
