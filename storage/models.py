from django.db import models


class UserFile(models.Model):
    author = models.ForeignKey('users.User', blank=True, null=True, default=None, on_delete=models.PROTECT)
    source = models.CharField(name='source', max_length=100, default='local')
    slug = models.UUIDField(name='slug', max_length=64)
    created_at = models.DateTimeField(auto_now_add=True)
    metadata = models.JSONField(default=dict)
