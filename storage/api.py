import json
from uuid import UUID

from django.http import Http404, HttpResponseBadRequest, HttpResponse
from django.views.generic import View

from storage.models import UserFile
from storage.storage import Storage


class UserFileView(View):
    def _get_record(self, slug: str) -> UserFile:
        if not slug:
            raise Http404

        try:
            slug = UUID(slug)
        except ValueError:
            raise ValueError()

        try:
            user_file = UserFile.objects.get(slug=slug)
        except UserFile.DoesNotExist:
            raise Http404

        return user_file

    def get_metadata(self, request, *args, **kwargs):
        try:
            user_file = self._get_record(request.GET.get('slug'))
        except ValueError:
            return HttpResponseBadRequest('Malformed slug')

        return HttpResponse(json.dumps(user_file.metadata), content_type='application/json')

    def get(self, request, *args, **kwargs):
        try:
            user_file = self._get_record(request.GET.get('slug'))
        except ValueError:
            return HttpResponseBadRequest('Malformed slug')

        storage = Storage()
        content = storage.get(user_file.slug)

        response = HttpResponse(
            content=content,
            content_type='application/binary',
        )
        response['Content-Disposition'] = f'attachment; filename="{ user_file.slug }"'
        return response

    def post(self, request, *args, **kwargs):
        metadata = request.POST

        try:
            content = request.FILES['content']
        except IndexError:
            return HttpResponseBadRequest('You have to send file in "content" field')

        storage = Storage()
        slug = storage.put(content.read())

        user_file = UserFile.objects.create(
            author=None,
            slug=slug,
            metadata=metadata,
        )

        return HttpResponse(json.dumps({"success": True, "slug": str(slug)}), content_type='application/json')
