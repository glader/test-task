from django.urls import path, re_path

from storage import api

app_name = 'seo'
urlpatterns = [
    re_path(r'^files$', api.UserFileView.as_view(), name='files'),
]
