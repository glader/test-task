from uuid import uuid4, UUID


class Storage:
    files = dict()

    def put(self, data: bytes) -> UUID:
        slug = uuid4()

        self.files[slug] = data
        return slug

    def get(self, slug: UUID) -> bytes:
        return self.files[slug]
